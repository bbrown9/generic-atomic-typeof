{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures   #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeOperators       #-}
module Main where

import           GHC.Generics
import           Type.Reflection

main :: IO ()
main = do
  putStrLn "List of base types in RecordType"
  print $ valTypesOf @RecordType
  putStrLn "List of base types in RecurseRecordType"
  print $ valTypesOf @RecurseRecordType
  putStrLn "List of base types in RecurseRecordType'"
  print $ valTypesOf @(RecurseRecordType' Int)
  putStrLn "List of base types in NestedRecordType"
  print $ valTypesOf @NestedRecordType


-- Type to investigate
data RecordType
  = MkRecordType
      { a :: Integer
      , b :: Char
      }
  deriving (Generic)

instance ValTypesOf RecordType

valRecord :: RecordType
valRecord = MkRecordType { a = 1, b = '2' }

data NestedRecordType
  = MkNestedRecordType
      { an :: Integer
      , bn :: RecordType
      }
  deriving (Generic)

instance ValTypesOf NestedRecordType

data RecurseRecordType
  = MkRecurseRecordType
      { aa :: Integer
      , bb :: RecurseRecordType
      }
  deriving (Generic)

instance ValTypesOf RecurseRecordType

data RecurseRecordType' a
  = MkRecurseRecordType'
      { aaa :: a
      , bbb :: RecurseRecordType' a
      }
  deriving (Generic)

instance (Typeable a) => ValTypesOf (RecurseRecordType' a)

-- Generic shenanigans

class ValTypesOf a where
  valTypesOf :: [String]
  default valTypesOf :: (Generic a, GValTypesOf (Rep a)) => [String]
  valTypesOf = gValTypesOf @(Rep a)

class GValTypesOf (f :: * -> *) where
  gValTypesOf :: [String]

instance (GValTypesOf f) => GValTypesOf (M1 D c f) where
  gValTypesOf = gValTypesOf @f

instance (GValTypesOf f, GValTypesOf g) => GValTypesOf (f :+: g) where
  gValTypesOf = gValTypesOf @f ++ gValTypesOf @g

instance (GValTypesOf f, GValTypesOf g) => GValTypesOf (f :*: g) where
  gValTypesOf = gValTypesOf @f ++ gValTypesOf @g

instance (GValTypesOf f) => GValTypesOf (S1 c f) where
  gValTypesOf = gValTypesOf @f

instance (GValTypesOf f) => GValTypesOf (C1 c f) where
  gValTypesOf = gValTypesOf @f

instance GValTypesOf V1 where
  gValTypesOf = []

instance GValTypesOf U1 where
  gValTypesOf = []

instance (Typeable c) => GValTypesOf (Rec0 c) where
  gValTypesOf = [show $ typeRep @c]
